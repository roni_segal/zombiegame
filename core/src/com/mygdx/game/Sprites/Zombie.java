package com.mygdx.game.Sprites;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Screens.SgateScreen;


/**
 * Created by ronio on 5/29/2017.
 */

public class Zombie {
    //private float x, y;
    private SgateScreen _screen;
    private float _movingSpeed;
    private float _rotation;
    private TextureRegion _textureRegion;
    private Vector2 _location;
    public final float DMG = 5;
    public final float ATTACK_COLLDOWN = 1.5f;
    public float attackColldown = 0f;
    public final float TOTAL_HP = 50f;
    private float HP = TOTAL_HP;
    public Rectangle _bounds;
    private boolean _setToDestroy = false;

    public Zombie(SgateScreen screen, float x, float y) {
        _screen = screen;
        _location = new Vector2();
        setX(x);
        setY(y);
        _movingSpeed = 20f;
        setRotation(0);
        _textureRegion = new TextureRegion(new Texture("Zombie1.png"));
        _bounds = new Rectangle(x - _textureRegion.getRegionWidth() / 2f, y - _textureRegion.getRegionHeight() / 2f, _textureRegion.getRegionWidth(), _textureRegion.getRegionHeight());
    }

    public void update(float dt) {
        //_screen.getViewport().unproject(touchPoint.set(_screen.getPlayer().getX(), _screen.getPlayer().getY(), 0)); // touchPoint now has the (x, y) of the mouse click
        // Aim:
        AimAt(_screen.getPlayer().getX(), _screen.getPlayer().getY());
        // Move:
        double dst = Math.sqrt(Math.pow(_screen.getPlayer().getX()-getX(),2)+Math.pow(_screen.getPlayer().getY()-getY(),2));
        if(dst > _screen.getPlayer().getTextureRegion().getRegionWidth() / 2) { // needs to move twords the player
            setX(getX() + (float) ((_screen.getPlayer().getX() - getX()) / dst) * getMovingSpeed() * dt);
            setY(getY() + (float) ((_screen.getPlayer().getY() - getY()) / dst) * getMovingSpeed() * dt);
        }
        else { // player is in attack range
            if(attackColldown >= ATTACK_COLLDOWN) {
                _screen.getPlayer().takeHit(DMG);
                attackColldown = 0f;
            }
        }
        attackColldown += dt;
        _bounds.setX(_location.x - _textureRegion.getRegionWidth() / 2f);
        _bounds.setY(_location.y - _textureRegion.getRegionHeight() / 2f);

    }

    public void draw(Batch batch) {
        batch.draw(
                getTextureRegion().getTexture(), // the texture
                getX() - getTextureRegion().getTexture().getWidth() / 2f, // start X
                getY() - getTextureRegion().getTexture().getHeight() / 2f, // start Y
                getTextureRegion().getTexture().getWidth()  / 2f, // originX
                getTextureRegion().getTexture().getHeight() / 2f, // originY
                getTextureRegion().getTexture().getWidth(), // Width
                getTextureRegion().getTexture().getHeight(), // Height
                1, // Scale X
                1, // Scale Y
                _rotation, // rotation
                getTextureRegion().getRegionX(), // srcX
                getTextureRegion().getRegionY(), // srcY
                getTextureRegion().getRegionWidth(), // srcWidth
                getTextureRegion().getRegionHeight(), // srcHeight
                false, // flip X
                false // flip Y
        );
    }

    public void takeDmg(float dmg) {
        HP -= dmg;
        if(HP <= 0) {
            _setToDestroy = true;
        }
    }

    public boolean equals(Zombie other) {
        return _location.x == other._location.x && _location.y == other._location.y &&
                HP == other.HP;
    }

    public float getX() {
        return _location.x;
    }

    public void setX(float x) {
        _location.x = x;
    }

    public float getY() {
        return _location.y;
    }

    public void setY(float y) {
        _location.y = y;
    }

    public float getRotation() {
        return _rotation;
    }

    public void setRotation(float rotation) {
        _rotation = rotation;
    }

    public void rotate(float deg) {
        _rotation += deg;
        _rotation = _rotation % 360f;
    }

    public TextureRegion getTextureRegion() {
        return _textureRegion;
    }

    public void dispose() {
        getTextureRegion().getTexture().dispose();
    }

    private void AimAt(float screenX, float screenY) {
        double degrees = Math.atan2(screenY - getY(), screenX - getX());
        degrees = degrees * 180.0d / Math.PI;
        degrees -= 90d;
        degrees = degrees - getRotation();
        while(degrees < -360) degrees += 360;
        degrees = degrees > 0? degrees: 360 + degrees;
        degrees = degrees%360;
        rotate((float)degrees);
    }

    private float getMovingSpeed() {
        return _movingSpeed;
    }

    public void drawHelpers(ShapeRenderer sr) {
        /*sr.point(getX(), getY(), 0);
        sr.rect(
                getX() - getTextureRegion().getTexture().getWidth() / 2f,
                getY() - getTextureRegion().getTexture().getHeight() / 2f,
                getTextureRegion().getTexture().getWidth(),
                getTextureRegion().getTexture().getHeight()
        );*/
        sr.rect(_bounds.getX(), _bounds.getY(), _bounds.getWidth(), _bounds.getHeight());

        // draw health bar:
        sr.set(ShapeRenderer.ShapeType.Filled);
        sr.setColor(new Color(255, 0, 0, 0));
        sr.rect(_location.x - getTexture().getWidth() / 2, _location.y - getTexture().getHeight() / 2 - 5, HP / TOTAL_HP * getTexture().getWidth(), 2.3f);
    }

    public Vector2 getLocation() {
        return _location;
    }

    public float getHP() {
        return HP;
    }

    public Rectangle getBounds() {
        return _bounds;
    }

    public boolean isSetToDestroy() {
        return _setToDestroy;
    }

    public Texture getTexture() {
        return _textureRegion.getTexture();
    }
}
