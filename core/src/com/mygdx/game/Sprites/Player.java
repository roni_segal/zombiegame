package com.mygdx.game.Sprites;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Screens.SgateScreen;
import com.mygdx.game.Weapons.Pistol;
import com.mygdx.game.Weapons.Weapon;

/**
 * Created by ronio on 5/29/2017.
 */

public class Player implements InputProcessor {
    //private float x, y;
    private SgateScreen _screen;
    private float _movingSpeed;
    private float _rotation;
    private TextureRegion _textureRegion;
    private boolean keySensorW, keySensorS, keySensorA, keySensorD;
    private Vector3 touchPoint = new Vector3();
    private Vector2 _location;
    private Vector2 _weaponLocation;
    private float HP;
    private boolean _fire;
    private Weapon _weapon;
    private float _aimAtX, _aimAtY;
    public Player(SgateScreen screen, float x, float y) {
        _screen = screen;
        _location = new Vector2();
        setX(x);
        setY(y);
        _movingSpeed = 80f;
        setRotation(0);
        _textureRegion = new TextureRegion(new Texture("player.png"));
        keySensorA = false;
        keySensorD = false;
        keySensorS = false;
        keySensorW = false;
        HP = 100f;
        _weapon = new Pistol(_screen, this);
        _weaponLocation = new Vector2();
        _fire = false;
    }

    public void update(float dt) {
        if(keySensorW)
            setY(getY() + getMovingSpeed() * dt);
        if(keySensorS)
            setY(getY() - getMovingSpeed() * dt);
        if(keySensorD)
            setX(getX() + getMovingSpeed() * dt);
        if(keySensorA)
            setX(getX() - getMovingSpeed() * dt);
        _weapon.update(dt);
        if(_fire) {
            _weapon.CanFire();
        }
    }

    public void draw(Batch batch) {
        batch.draw(
                getTextureRegion().getTexture(), // the texture
                getX() - getTextureRegion().getTexture().getWidth() / 2f, // start X
                getY() - getTextureRegion().getTexture().getHeight() / 2f, // start Y
                getTextureRegion().getTexture().getWidth()  / 2f, // originX
                getTextureRegion().getTexture().getHeight() / 2f, // originY
                getTextureRegion().getTexture().getWidth(), // Width
                getTextureRegion().getTexture().getHeight(), // Height
                1, // Scale X
                1, // Scale Y
                _rotation, // rotation
                getTextureRegion().getRegionX(), // srcX
                getTextureRegion().getRegionY(), // srcY
                getTextureRegion().getRegionWidth(), // srcWidth
                getTextureRegion().getRegionHeight(), // srcHeight
                false, // flip X
                false // flip Y
        );
        _weapon.draw(batch);
    }

    public void drawHelpers(ShapeRenderer sr) {
        /*sr.point(getX(), getY(), 0);
        sr.rect(
                getX() - getTextureRegion().getTexture().getWidth() / 2f,
                getY() - getTextureRegion().getTexture().getHeight() / 2f,
                getTextureRegion().getTexture().getWidth(),
                getTextureRegion().getTexture().getHeight()
        );*/
        _weapon.drawHelpers(sr);
    }

    public float getX() {
        return _location.x;
    }

    public void setX(float x) {
        _location.x = x;
    }

    public float getY() {
        return _location.y;
    }

    public float getAimAtX() {
        return _aimAtX;
    }

    public float getAimAtY() {
        return _aimAtY;
    }

    public void setY(float y) {
        _location.y = y;
    }

    public float getRotation() {
        return _rotation;
    }

    public void setRotation(float rotation) {
        _rotation = rotation;
    }

    public void rotate(float deg) {
        _rotation += deg;
        _rotation = _rotation % 360f;
    }

    public TextureRegion getTextureRegion() {
        return _textureRegion;
    }

    public void dispose() {
        getTextureRegion().getTexture().dispose();
    }

    private void AimAt(float screenX, float screenY) {
        _aimAtX = screenX;
        _aimAtY = screenY;
        double degrees = Math.atan2(screenY - getY(), screenX - getX());
        degrees = degrees * 180.0d / Math.PI;
        degrees -= 90d;
        degrees = degrees - getRotation();
        while(degrees < -360) degrees += 360;
        degrees = degrees > 0? degrees: 360 + degrees;
        degrees = degrees%360;
        rotate((float)degrees);
    }

    private float getMovingSpeed() {
        return _movingSpeed;
    }

    public Vector2 getLocation() {
        return _location;
    }

    public Weapon getWeapon() {
        return _weapon;
    }







    // InputProcessor
    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.W:
                keySensorW = true;
                break;
            case Input.Keys.S:
                keySensorS = true;
                break;
            case Input.Keys.A:
                keySensorA = true;
                break;
            case Input.Keys.D:
                keySensorD = true;
                break;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.W:
                keySensorW = false;
                break;
            case Input.Keys.S:
                keySensorS = false;
                break;
            case Input.Keys.A:
                keySensorA = false;
                break;
            case Input.Keys.D:
                keySensorD = false;
                break;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(Input.Buttons.LEFT == button) {
            _fire = true;
            return true;
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(Input.Buttons.LEFT == button) {
            _fire = false;
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        mouseMoved(screenX, screenY);
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        _screen.getViewport().unproject(touchPoint.set(screenX, screenY, 0)); // touchPoint now has the (x, y) of the mouse click
        AimAt(touchPoint.x, touchPoint.y);
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public float getHP() {
        return HP;
    }

    public void setHP(float HP) {
        this.HP = HP;
    }

    public void takeHit(float amount) {
        setHP(getHP() - amount);
        if(getHP() <= 0) {
            _screen.gameOver();
        }
    }
}
