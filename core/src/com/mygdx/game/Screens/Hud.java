package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.ZombieGame;

import static com.mygdx.game.ZombieGame.VIEWPORT_HEIGHT;
import static com.mygdx.game.ZombieGame.VIEWPORT_WIDTH;
import static java.lang.System.exit;

/**
 * Created by ronio on 5/29/2017.
 */
public class Hud implements Disposable {
    public Stage _stage;
    private Viewport _viewport;
    private Label HPLabel;
    private Skin _skin;
    private SgateScreen _screen;

    // health bar stats:
    private final float HEALTHBAR_MAX_WIDTH = VIEWPORT_WIDTH / 1.33333333333f;
    private final float HEALTHBAR_MAX_HEIGHT = VIEWPORT_HEIGHT / 11.25f;
    private final float HEALTHBAR_START_X = VIEWPORT_WIDTH / 8;
    private final float HEALTHBAR_START_Y = VIEWPORT_HEIGHT - HEALTHBAR_MAX_HEIGHT - 2f;

    // meg stats:
    private final float MEGSTAT_MAX_WIDTH = VIEWPORT_WIDTH / 7f;
    private final float MEGSTAT_MAX_HEIGHT = VIEWPORT_HEIGHT / 17f;
    private final float MEGSTAT_START_X = HEALTHBAR_START_X; /*VIEWPORT_WIDTH / 12f;*/
    private final float MEGSTAT_START_Y =HEALTHBAR_START_Y - HEALTHBAR_MAX_HEIGHT - MEGSTAT_MAX_HEIGHT - 2f;


    public Hud(SpriteBatch batch, SgateScreen screen) {
        _viewport = new FitViewport(ZombieGame.VIEWPORT_WIDTH, VIEWPORT_HEIGHT, new OrthographicCamera()); // set the viewport size
        _stage = new Stage(_viewport, batch);
        _screen = screen;


        if(Gdx.files.internal("UI/uiskin.json").exists()) {
            _skin = new Skin(Gdx.files.internal("UI/uiskin.json"));
        } else {
            Gdx.app.log("", "uniskin.json file not exists");
            exit(1);
        }

        HPLabel = new Label("temp", _skin);

        Table table = new Table();
        table.top();
        table.setFillParent(true);
        table.add(HPLabel);
        _stage.addActor(table);
    }

/*        TextureRegion textureRegion = new TextureRegion(new Texture(Gdx.files.internal("UI/barGreen_horizontalMid.png")));
        textureRegion.setRegionWidth(10);
        textureRegion.setRegionHeight(10);
        TextureRegionDrawable textureBar = new TextureRegionDrawable(textureRegion);
        //ProgressBar.ProgressBarStyle barStyle = new ProgressBar.ProgressBarStyle(_skin.newDrawable("white", Color.DARK_GRAY), textureBar);
        ProgressBar.ProgressBarStyle barStyle = new ProgressBar.ProgressBarStyle(_skin.newDrawable("white", Color.RED),       textureBar);
        barStyle.knobBefore = barStyle.knob;
        barStyle.
        bar = new ProgressBar(0, 100, 1, false, barStyle);
        table.add(bar);

        //HPLabel = new Label(String.format("%03d", 100), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        /*HP = new Slider(0, 100, 1, false, _skin);
        HP.setVisible(true);
        table.add(HP); */

    public void update(float dt) {
        // TODO: change it to something that didn't called every frame:
        //HPLabel.setText(String.format("%03d", (int)_screen.getPlayer().getHP()));
        HPLabel.setText("" + (int)_screen.getPlayer().getHP());
        //_stage.act();
    }

    public void drawHelpers(ShapeRenderer sr) {
        sr.setProjectionMatrix(_viewport.getCamera().combined);
        sr.set(ShapeRenderer.ShapeType.Filled);

        // draw health bar:
        sr.setColor(Color.RED);
        sr.rect(HEALTHBAR_START_X - 0.5f,HEALTHBAR_START_Y - 0.5f, HEALTHBAR_MAX_WIDTH + 1, HEALTHBAR_MAX_HEIGHT + 1);
        sr.setColor(Color.GREEN);
        // TODO: check for creasing
        sr.rect(HEALTHBAR_START_X, HEALTHBAR_START_Y, HEALTHBAR_MAX_WIDTH / (100f / _screen.getPlayer().getHP()), HEALTHBAR_MAX_HEIGHT);
        // draw meg, meg size and reload:
        sr.setColor(Color.YELLOW);
        sr.rect(MEGSTAT_START_X - 0.3f, MEGSTAT_START_Y - 0.3f, MEGSTAT_MAX_WIDTH + 0.6f, MEGSTAT_MAX_HEIGHT + 0.6f);
        if(_screen.getPlayer().getWeapon().isReloading()) {
            sr.setColor(Color.RED);
            sr.rect(MEGSTAT_START_X, MEGSTAT_START_Y, MEGSTAT_MAX_WIDTH / (_screen.getPlayer().getWeapon().getReloadTime() / _screen.getPlayer().getWeapon().getCurrReload()), MEGSTAT_MAX_HEIGHT);
        }
        else {
            sr.setColor(Color.BLUE);
            sr.rect(MEGSTAT_START_X, MEGSTAT_START_Y, MEGSTAT_MAX_WIDTH / ((float) _screen.getPlayer().getWeapon().getMegSize() / (float) _screen.getPlayer().getWeapon().getCurrMegSize()), MEGSTAT_MAX_HEIGHT);
        }
    }

    @Override
    public void dispose() {
        _stage.dispose();
    }

    public Stage getStage() {
        return _stage;
    }
}
