package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Sprites.Player;
import com.mygdx.game.Sprites.Zombie;
import com.mygdx.game.ZombieGame;

import java.util.Iterator;
import java.util.Vector;

import sun.util.resources.cldr.sg.CalendarData_sg_CF;

/**
 * Created by ronio on 5/26/2017.
 */

public class SgateScreen implements Screen {
    private ZombieGame _game;
    private OrthographicCamera _gamecam;
    private FitViewport _viewport;
    private Texture _background;
    private Vector<Zombie> _zombies;
    private Player _player;
    private ShapeRenderer _shapeRenderer;
    private Hud _hud;
    private Viewport _hudViewport;


    public SgateScreen(ZombieGame game) {
        _game = game;
        _gamecam = new OrthographicCamera(); // set the world size
        _gamecam.setToOrtho(false, ZombieGame.VIEWPORT_WIDTH, ZombieGame.VIEWPORT_HEIGHT);
        _viewport = new FitViewport(ZombieGame.VIEWPORT_WIDTH, ZombieGame.VIEWPORT_HEIGHT, _gamecam); // set the viewport size
        //_gamecam.position.set(ZombieGame.VIEWPORT_WIDTH / 2f, ZombieGame.VIEWPORT_HEIGHT / 2f, 0); // set the camera to be in the middle of the viewport
        _background = new Texture("background.png"); // load level1 (background) image
        _player = new Player(this, 100, 100);
        _zombies = new Vector<>();
        _zombies.add(new Zombie(this, 120, 120));
        _shapeRenderer = new ShapeRenderer();
        _hudViewport = new FitViewport(ZombieGame.VIEWPORT_WIDTH, ZombieGame.VIEWPORT_HEIGHT, new OrthographicCamera()); // set the viewport size
        _hud = new Hud(_game.batch, this);
        Gdx.input.setInputProcessor(_player);

    }

    @Override
    public void show() {

    }

    public void update(float dt) {
        _player.update(dt);
        for(Zombie zombie: _zombies) {
            zombie.update(dt);
        }

        Iterator<Zombie> iter = _zombies.iterator();
        while(iter.hasNext()) {
            if(iter.next().isSetToDestroy()) {
                iter.remove();
            }
        }

        _gamecam.position.set(_player.getX(), _player.getY(), 0);
        _hud.update(dt);
    }

    @Override
    public void render(float delta) {
        update(delta);
        _gamecam.update();
        Gdx.gl.glClearColor(0,1,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        _game.batch.setProjectionMatrix(_gamecam.combined);
        _game.batch.begin();
        _game.batch.draw(_background, 0, 0, ZombieGame.BACKGROUND_WIDTH, ZombieGame.BACKGROUND_HEIGHT);
        _player.draw(_game.batch);
        for(Object o: _zombies) {
            Zombie zombie = (Zombie) o;
            zombie.draw(_game.batch);
        }
        _game.batch.end();
        drawHelpers();
        _hud.getStage().draw();
    }

    private void drawHelpers() {
        _shapeRenderer.setProjectionMatrix(_gamecam.combined);
        _shapeRenderer.setAutoShapeType(true);
        _shapeRenderer.begin();
        for(Object o: _zombies) {
            Zombie zombie = (Zombie) o;
            zombie.drawHelpers(_shapeRenderer);
        }
        _player.drawHelpers(_shapeRenderer);
        _hud.drawHelpers(_shapeRenderer);
        _shapeRenderer.end();

    }

    @Override
    public void resize(int width, int height) {
        _viewport.update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        _player.dispose();
        for(Object o: _zombies) {
            Zombie zombie = (Zombie) o;
            zombie.dispose();
        }
        _hud.dispose();
    }

    public Viewport getViewport() {
        return _viewport;
    }
    public Camera getCamera() {
        return _gamecam;
    }

    public Player getPlayer() {
        return _player;
    }

    public ShapeRenderer getShapeRenderer() {
        return _shapeRenderer;
    }

    public void gameOver() {
        Gdx.app.exit();
    }

    public Vector<Zombie> getZombies() {
        return _zombies;
    }
}
