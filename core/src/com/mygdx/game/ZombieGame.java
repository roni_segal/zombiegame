package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Screens.SgateScreen;

public class ZombieGame extends Game {
	// level1.png image size: 1563x224
	// viewPort size: 300x224
	public static final float BACKGROUND_WIDTH = 750f;
	public static final float BACKGROUND_HEIGHT = 750f;
	public static final float RATIO = (16f / 9f);
	public static final float VIEWPORT_WIDTH = 400f;
	public static final float VIEWPORT_HEIGHT = VIEWPORT_WIDTH / RATIO;

	public static SpriteBatch batch;

	@Override
	public void create () {
		batch = new SpriteBatch();
		setScreen(new SgateScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
