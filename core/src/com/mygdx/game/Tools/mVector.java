package com.mygdx.game.Tools;

import java.util.Collection;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by ronio on 6/15/2017.
 */

public class mVector<E> extends Vector {
    Lock lock;
    public mVector() {
        super();
        lock = new ReentrantLock();
    }

    @Override
    public synchronized void setSize(int i) {
        lock.lock();
        super.setSize(i);
        lock.unlock();
    }

    @Override
    public synchronized int capacity() {
        lock.lock();
        int ret = super.capacity();
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized int size() {
        lock.lock();
        int  ret = super.size();
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized boolean isEmpty() {
        lock.lock();
        boolean ret = super.isEmpty();
        lock.unlock();
        return ret;
    }

    @Override
    public boolean contains(Object o) {
        lock.lock();
        boolean ret = super.contains(o);
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized Object elementAt(int i) {
        lock.lock();
        Object ret = super.elementAt(i);
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized Object firstElement() {
        lock.lock();
        Object ret = super.firstElement();
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized Object lastElement() {
        lock.lock();
        Object ret = super.lastElement();
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized void setElementAt(Object o, int i) {
        lock.lock();
        super.setElementAt(o, i);
        lock.unlock();
    }

    @Override
    public synchronized void removeElementAt(int i) {
        lock.lock();
        super.removeElementAt(i);
        lock.unlock();
    }

    @Override
    public synchronized void insertElementAt(Object o, int i) {
        lock.lock();
        super.insertElementAt(o, i);
        lock.unlock();
    }

    @Override
    public synchronized void addElement(Object o) {
        lock.lock();
        super.addElement(o);
        lock.unlock();
    }

    @Override
    public synchronized boolean removeElement(Object o) {
        lock.lock();
        boolean ret = super.removeElement(o);
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized void removeAllElements() {
        lock.lock();
        super.removeAllElements();
        lock.unlock();
    }

    @Override
    public synchronized Object get(int i) {
        lock.lock();
        Object ret = super.get(i);
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized Object set(int i, Object o) {
        lock.lock();
        Object ret = super.set(i, o);
        lock.unlock();
        return ret;
    }

    @Override
    public synchronized boolean add(Object o) {
        lock.lock();
        boolean ret = super.add(o);
        lock.unlock();
        return ret;
    }

    @Override
    public boolean remove(Object o) {
        lock.lock();
        boolean ret = super.remove(o);
        lock.unlock();
        return ret;
    }

    @Override
    public void add(int i, Object o) {
        lock.lock();
        super.add(i, o);
        lock.unlock();
    }

    @Override
    public synchronized Object remove(int i) {
        lock.lock();
        Object ret = super.remove(i);
        lock.unlock();
        return ret;
    }

    @Override
    public void clear() {
        lock.lock();
        super.clear();
        lock.unlock();
    }

    @Override
    public synchronized boolean removeAll(Collection collection) {
        lock.lock();
        boolean ret = super.removeAll(collection);
        lock.unlock();
        return ret;
    }
}
