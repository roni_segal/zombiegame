package com.mygdx.game.Weapons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.Screens.SgateScreen;
import com.mygdx.game.Sprites.Player;
import com.mygdx.game.Weapons.Projectiles.NormalProjectile;

/**
 * Created by ronio on 5/29/2017.
 */

public class Pistol extends Weapon{
    //private TextureRegion _textureRegion;
    private float dmg = 20f;

    public Pistol(SgateScreen screen, Player owner) {
        //    cool  meg reload
        super(0.2f, 7, 1, screen, owner, new TextureRegion(new Texture("pistol.png")));
    }

    public void update(float dt) {
        super.update(dt);
    }

    @Override
    public void draw(Batch batch) {
        super.draw(batch);
        // TODO: make it look batter
        batch.draw(
                getTextureRegion().getTexture(), // the texture
                _location.x - getTextureRegion().getTexture().getWidth() / 2f, // start X
                _location.y - getTextureRegion().getTexture().getHeight() / 2f, // start Y
                getTextureRegion().getTexture().getWidth()  / 2f, // originX
                getTextureRegion().getTexture().getHeight() / 2f, // originY
                getTextureRegion().getTexture().getWidth(), // Width
                getTextureRegion().getTexture().getHeight(), // Height
                1, // Scale X
                1, // Scale Y
                _owner.getRotation(), // rotation
                getTextureRegion().getRegionX(), // srcX
                getTextureRegion().getRegionY(), // srcY
                getTextureRegion().getRegionWidth(), // srcWidth
                getTextureRegion().getRegionHeight(), // srcHeight
                false, // flip X
                false // flip Y
        );
    }


    //int i = 0;

    @Override
    protected void Fire() {
        _shotAt.set(_owner.getAimAtX(), _owner.getAimAtY());
        _projectiles.add(new NormalProjectile(_screen, dmg, _shotAt));
        /*Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss:SS");
        Gdx.app.log(++i + ") " + sdf.format(cal.getTime()), "Firing!!!");*/
    }
}
