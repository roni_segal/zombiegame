package com.mygdx.game.Weapons;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Screens.SgateScreen;
import com.mygdx.game.Sprites.Player;
import com.mygdx.game.Weapons.Projectiles.BasicProjectile;

import java.util.Vector;


/**
 * Created by ronio on 5/29/2017.
 */

public abstract class Weapon{
    protected float _cooldown;
    protected float _currCooldown;
    protected int _megSize;
    protected float _reloadTime;
    protected SgateScreen _screen;
    protected Vector2 _location;
    private float OFFSET;
    protected Player _owner;
    private TextureRegion _textureRegion;
    private boolean isReloading;
    private int _currMegSize;
    private float _currReload;
    protected Vector<BasicProjectile> _projectiles;
    protected Vector2 _shotAt = new Vector2();


    public Weapon(float cooldown, int megSize, float reloadTime, SgateScreen screen, Player owner, TextureRegion textureRegion) {
        this._textureRegion = textureRegion;
        this._cooldown = cooldown;
        this._currCooldown = 0f;
        this._currReload = 0f;
        this._currMegSize = megSize;
        this._megSize = megSize;
        this._reloadTime = reloadTime;
        this._screen = screen;
        this._location = new Vector2();
        this._owner = owner;
        this.isReloading = false;
        this._projectiles = new Vector<>();

        OFFSET = _owner.getTextureRegion().getRegionHeight() / 4f;
    }

    public void update(float dt) {
        _currCooldown += dt;
        _currReload += dt;
        if(_currReload >= _reloadTime) {
            isReloading = false;
        }

        _location.x = _screen.getPlayer().getX() + OFFSET * MathUtils.cosDeg(_screen.getPlayer().getRotation() + 90);
        _location.y = _screen.getPlayer().getY() + OFFSET * MathUtils.sinDeg(_screen.getPlayer().getRotation() + 90);
        for(int i = 0; i < _projectiles.size();i++) {
            _projectiles.elementAt(i).update(dt);
            if(_projectiles.elementAt(i).isSetToDestroy())
                _projectiles.removeElementAt(i);
        }
    }

    public void draw(Batch batch) {
        for(BasicProjectile projectile: _projectiles)
            projectile.draw(batch);
    }

    public void drawHelpers(ShapeRenderer sr) {
        sr.setColor(Color.BLUE);
        sr.point(_location.x, _location.y, 0);
        sr.setColor(Color.RED);
        for(BasicProjectile projectile: _projectiles)
            projectile.drawHelpers(sr);
    }

    public TextureRegion getTextureRegion() {
        return _textureRegion;
    }


    // fires if it cans
    public void CanFire() {
        if(_currCooldown >= _cooldown && !isReloading) {
            Fire();
            _currCooldown = 0f; // reset cool down
            _currMegSize -= 1;
            if(_currMegSize == 0) {
                isReloading = true;
                _currReload = 0f;
                _currMegSize = _megSize;
            }
        }
    }

    protected abstract void Fire();


    public int getMegSize() {
        return _megSize;
    }
    public int getCurrMegSize() {
        return _currMegSize;
    }

    public float getReloadTime() {
        return _reloadTime;
    }

    public float getCurrReload() {
        return _currReload;
    }

    public boolean isReloading() {
        return isReloading;
    }

    public Vector2 getLocation() {
        return _location;
    }

    public Vector<BasicProjectile> getProjectiles() {
        return _projectiles;
    }
}
