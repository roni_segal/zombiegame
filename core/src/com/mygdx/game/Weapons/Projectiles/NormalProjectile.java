package com.mygdx.game.Weapons.Projectiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Screens.SgateScreen;

/**
 * Created by ronio on 6/9/2017.
 */

public class NormalProjectile extends  BasicProjectile {
    public NormalProjectile(SgateScreen screen, float dmg, Vector2 shotAt) {
        super(screen, new TextureRegion(new Texture("bullet.png"), 2, 2), dmg, shotAt);
    }

    @Override
    public void update(float dt) {
        super.update(dt);
    }

    @Override
    public void draw(Batch batch) {
        batch.draw(getTextureRegion(),
                _location.x - getTextureRegion().getRegionWidth() / 2f,
                _location.y - getTextureRegion().getRegionWidth() / 2f);
    }

    @Override
    public void drawHelpers(ShapeRenderer sr) {
        //sr.circle(_location.x, _location.y, 5);
    }
}
