package com.mygdx.game.Weapons.Projectiles;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Screens.SgateScreen;
import com.mygdx.game.Sprites.Zombie;

import java.util.Vector;


/**
 * Created by ronio on 6/9/2017.
 */

public abstract class BasicProjectile {
    protected SgateScreen _screen;
    protected Vector2 _direction;
    protected Vector2 _location;
    protected Rectangle _bounds;
    protected float _speed = 1000f;
    protected float dmg;
    private boolean setToDestroy;
    protected TextureRegion _textureRegion;

    public BasicProjectile(SgateScreen screen, TextureRegion texture, float dmg, Vector2 shotAt) {
        _screen = screen;
        this.dmg= dmg;
        setToDestroy = false;
        _location = new Vector2(_screen.getPlayer().getWeapon().getLocation().x, _screen.getPlayer().getWeapon().getLocation().y);
        _bounds = new Rectangle(_location.x, _location.y, 2, 2);
        _textureRegion = texture;

        // calculate the direction of the projectile
        float pathX = shotAt.x - _location.x;
        float pathY = shotAt.y - _location.y;

        float distance = (float) Math.sqrt(pathX * pathX + pathY * pathY);
        _direction = new Vector2(pathX / distance, pathY / distance);
    }

    public void update(float dt) {
        _location.set(_location.x + (_direction.x * _speed * dt), _location.y + (_direction.y * _speed * dt));
        _bounds.setX(_location.x);
        _bounds.setY(_location.y);
        Vector<Zombie> zombies = _screen.getZombies();
        for(Zombie zombie: zombies) {
            if(zombie.getBounds().overlaps(getBounds())) {
                zombie.takeDmg(dmg);
                setToDestroy = true;
            }
        }
    }
    public abstract void draw(Batch batch);
    public void drawHelpers(ShapeRenderer sr) {
        sr.rect(_bounds.getX() - _bounds.getWidth() / 2f, _bounds.getY() - _bounds.getHeight() / 2f, _bounds.getWidth(), _bounds.getHeight());
    }

    public Rectangle getBounds() {
        return _bounds;
    }

    public boolean equals(BasicProjectile other) {
        if(this._direction == other._direction && this.dmg == other.dmg && this._speed == other._speed && this._location == other._location) {
            return true;
        }
        return false;
    }

    public boolean isSetToDestroy() {
        return setToDestroy;
    }

    public TextureRegion getTextureRegion() {
        return _textureRegion;
    }
}
